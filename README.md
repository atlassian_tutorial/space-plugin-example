# Tutorial: Adding Space Shortcut Links

This plugin is an example quoted on [Adding Space Shortcut Links][1] to illustrate the new Sidebar links for Confluence 5.0.

It shows:

* How to create Quick Links in the Space Sidebar,
* How to create Main Links,
* How to create Main Links using a Context Provider,
* How to create a Space Advanced screen,
* How to create a Space Admin screen.

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

    atlas-mvn confluence:run

 [1]: https://developer.atlassian.com/display/CONFDEV/Adding+Space+Shortcut+Links
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project