package com.atlassian.examples;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;

import static java.util.Objects.requireNonNull;

public class ViewLinkServlet extends HttpServlet {
    private static final String SPACE_ID_KEY = "spaceId";
    private final UserManager userManager;
    private final UserAccessor userAccessor;
    private final LoginUriProvider loginUriProvider;
    private final SpaceManager spaceManager;
    private final TemplateRenderer templateRenderer;

    public ViewLinkServlet(UserManager userManager, UserAccessor userAccessor, LoginUriProvider loginUriProvider,
                           SpaceManager spaceManager, TemplateRenderer templateRenderer) {
        this.userManager = requireNonNull(userManager);
        this.userAccessor = requireNonNull(userAccessor);
        this.loginUriProvider = requireNonNull(loginUriProvider);
        this.spaceManager = requireNonNull(spaceManager);
        this.templateRenderer = requireNonNull(templateRenderer);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Find current user
        UserKey userKey = userManager.getRemoteUserKey(request);
        ConfluenceUser user = userAccessor.getExistingUserByKey(userKey);
        if (userKey == null || user == null) {
            redirectToLogin(request, response);
            return;
        }

        // Find space in the request
        Space space = null;
        String spaceIdString = request.getParameter(SPACE_ID_KEY);
        if (spaceIdString != null) {
            try {
                long spaceId = Long.parseLong(spaceIdString);
                space = spaceManager.getSpace(spaceId);
            } catch (NumberFormatException nfe) {
                // log a warning if needed
            }
        }

        if (space == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        response.setContentType("text/html;charset=utf-8");
        templateRenderer.render("/templates/view-link-action.vm",
                ImmutableMap.of("spaceId", space.getId()),
                response.getWriter());

    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }
}
