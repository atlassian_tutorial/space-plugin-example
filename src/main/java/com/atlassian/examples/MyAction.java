package com.atlassian.examples;

import com.atlassian.confluence.plugins.ia.service.SidebarLinkService;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAction;

public class MyAction extends AbstractSpaceAction
{
    private SidebarLinkService sidebarLinkService;
    private String message;
    private String confirm;

    public String doCreate() throws Exception
    {
        if (!"true".equals(confirm))
        {
            message = "Please confirm you want to add a link to this space.";
            return INPUT;
        }
        /*
         * Note: It is not recommended to perform actions in the doDefault() method. This is only
         * intended as an example to use the Sidebar API.
         */

        // Get the ID of a page in the space
        Space space = getSpace();
        if (space == null)
        {
            message = "The Space wasn't found.";
            return INPUT;
        }
        long id = space.getHomePage().getId();

        // Check whether the Quick Link already exists
        boolean exists = sidebarLinkService.hasQuickLink(this.getSpaceKey(), id);

        // If not, create the link using the ID of the page.
        // By default, the title will be the title of the page.
        if (!exists)
        {
            sidebarLinkService.create(getSpaceKey(), id, null, null);
            message = "Link added";
        }
        else
        {
            message = "Link already exists";
        }

        return SUCCESS;
    }

    public String doViewCustomContent()
    {
        return INPUT;
    }

    public void setSidebarLinkService(SidebarLinkService sidebarLinkService)
    {
        this.sidebarLinkService = sidebarLinkService;
    }

    public String getMessage()
    {
        return message;
    }

    public String getConfirm()
    {
        return confirm;
    }

    public void setConfirm(String confirm)
    {
        this.confirm = confirm;
    }
}

