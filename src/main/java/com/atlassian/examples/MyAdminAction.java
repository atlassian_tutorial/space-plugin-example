package com.atlassian.examples;

import com.atlassian.confluence.spaces.actions.SpaceAdminAction;

public class MyAdminAction extends SpaceAdminAction
{
    @Override
    public String doDefault()
    {
        return INPUT;
    }
}
