package com.atlassian.examples;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.util.Collections;
import java.util.Map;

public class ContextualMainLinkContextProvider implements ContextProvider
{

    @Override
    public void init(Map<String, String> map) throws PluginParseException
    {
        // Nothing to initialise
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> map) {
        Space space = (Space) map.get("space");
        return ImmutableMap.<String, Object>of("generatedLink", "/dosearchsite.action?queryString=" + space.getName());
    }
}