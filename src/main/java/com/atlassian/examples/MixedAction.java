package com.atlassian.examples;

import com.atlassian.confluence.spaces.actions.SpaceAdminAction;

public class MixedAction extends SpaceAdminAction
{
    @Override
    public String doDefault()
    {
        return INPUT;
    }
}